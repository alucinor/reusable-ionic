import React, { Component } from 'react';

import logo from './logo.svg';
import './App.css';


window._sample_month_search = {
  Search: async function( q ) {
    return new Promise( ( resolve, reject ) => {
      fetch( '/q/' + encodeURIComponent( q ) )
        .then( ( rep ) => rep.text() )
        .then( ( results ) => {
          resolve( results.trim().split( '\n' ) );
        } )
        .catch( reject );
    } );
  }
};

export class App extends Component {

  constructor( props ) {
    super( props );
    this.state = {
      selected: ''
    };
  }

  componentDidMount() {
    this.refs.libSearch.addEventListener(
      'result-selected', ( e ) => {
        /*DEBUG*/console.log( 'react app received event' ); //console.log( e );
        this.setState( { selected: e.detail } );
      } );
  }

  render() {
    return(
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <lib-search search-agent='_sample_month_search' ref='libSearch'></lib-search>
          <p>{this.state.selected}</p>
        </header>
      </div>
    )
  }

}

export default App;

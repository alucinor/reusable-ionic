package main

import (
	"bufio"
	"embed"
	"fmt"
	"io/fs"
	"net/http"
	"strings"

	goji "goji.io"
	"goji.io/pat"
)

//go:embed sample-react-app/build
var reactApp embed.FS

//go:embed fruit.txt
var fruitTerms string

func main() {
	fmt.Println("sample server")

	var fruit []string
	scanner := bufio.NewScanner(strings.NewReader(fruitTerms))
	for scanner.Scan() {
		fruit = append(fruit, strings.ToLower(scanner.Text()))
	}

	mux := goji.NewMux()
	mux.HandleFunc(
		pat.Get("/q/:query"),
		func(rep http.ResponseWriter, req *http.Request) {
			q := strings.TrimSpace(strings.ToLower(pat.Param(req, "query")))
			for _, f := range fruit {
				if strings.HasPrefix(f, q) {
					fmt.Fprintln(rep, f)
				}
			}
		})

	reactAppBuild, _ := fs.Sub(reactApp, "sample-react-app/build")
	mux.Handle(pat.Get("/*"), http.FileServer(http.FS(reactAppBuild)))

	addr := "localhost:8080"
	fmt.Printf("Listening on %s.\n", addr)
	http.ListenAndServe(addr, mux)
}

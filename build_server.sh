#!/bin/sh -x


echo "Building core web component."

cd core
rm -rf dist ; rm -rf www
npm install
npm run build
cd ..


echo "Building sample React app."

cd server/sample-react-app
rm -rf build
npm install
npm run build
cd ..


echo "Building sample server."

_BUILD="go build"
_EXCMDNAME=sample-reuse-server
_MAIN=main.go

rm -rf ../dist ; mkdir ../dist

go mod vendor

mkdir -p ../dist/mac
GOOS=darwin GOARCH=amd64 $_BUILD -o ../dist/mac/$_EXCMDNAME $_MAIN

mkdir -p ../dist/linux
GOOS=linux GOARCH=amd64 $_BUILD -o ../dist/linux/amd64/$_EXCMDNAME $_MAIN

mkdir -p ../dist/win
GOOS=windows GOARCH=amd64 $_BUILD -o ../dist/linux/amd64/$_EXCMDNAME.exe $_MAIN

cd ../..

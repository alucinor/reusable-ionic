# reusable-ionic

Sample projects demonstrating UI reuse between an Ionic-based mobile application and a React-based desktop web application.

## core

The `core` directory contains a [Stencil](http://stenciljs.com) project that implements the `lib-search` web component, assembled from a few Ionic Framework components.

The component expect to be supplied with a `SearchAgent` implementation, which it will use to perform searches from user input.

## ionic-angular

The `ionic-angular` directory contains an [Ionic Framework](http://ionicframework.com) v3 application that consumes the `lib-search` component.

This project represents a hybrid mobile application, and supplies a `SearchAgent` implementation that performs in-memory searches against an array of values.

## server/sample-react-app

The `sample-react-app` project uses the `lib-search` component in a simple [React](https://reactjs.org) application.

This project represents an always-online desktop web application. It supplies a `SearchAgent` implementation that performs searches against an HTTP endpoint (a compatible server is included in the outer `server` project).

The server-hosted `sample-react-app` project, including the `lib-search` web component, React application, and Go server, can be built by the `build_server.sh` script.
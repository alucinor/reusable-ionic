import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { SearchAgent } from 'sample-pluggable-component/dist/types/components/lib-search/lib-search';

const sampleMonthSearchData =
  [ 'January', 'February', 'March', 'April', 'May', 'June', 'July',
    'August', 'September', 'October', 'November', 'December' ];

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  agent: SearchAgent = {
    Search: ( query ) => {
      return new Promise(
        ( resolve, reject ) => {
          let results = [];
          for ( let d of sampleMonthSearchData ) {
            if ( d.toLowerCase().startsWith( query ) ) {
              results.push( d );
            }
          }
          resolve( results );
        } );
    }
  };

  constructor(public navCtrl: NavController) {

  }

}

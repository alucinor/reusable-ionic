import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { defineCustomElements } from 'sample-pluggable-component/loader';

import { AppModule } from './app.module';

platformBrowserDynamic().bootstrapModule(AppModule);
defineCustomElements( window, {
  exclude: [
    'ion-app', 'ion-menu', 'ion-header', 'ion-toolbar', 'ion-navbar'
  ]
} );

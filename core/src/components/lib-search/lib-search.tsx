import { Component, Event, EventEmitter, Host, h, Prop, State, Watch } from '@stencil/core';

/*
    UI components from the Ionic Framework are used here for their mobile
    compatibility. This component can be integrated into myUnity Mobile and
    other Ionic-based apps with little change. Desktop web consumers can
    conform the component to blend in with their look and feel through CSS.
 */
import '@ionic/core';
import { IonSearchbar } from '@ionic/core/components/ion-searchbar';

/*
    SearchAgent interface definition for use by consumers written in TypeScript.
 */
export interface SearchAgent {
  Search: (query: string) => Promise<string[]>;
}

/*
    StencilJS implementation of lib-search web component. StencilJS itself is
    not a required dependency for consumers; it compiles components down to
    framework-independent JavaScript.
 */
@Component({
  tag: 'lib-search',
  styleUrl: 'lib-search.css',
  shadow: false,
})
export class LibSearch {

  /**
   * SearchAgent will be used to perform search queries; string values are
   * used as keys for retrieving SearchAgent from the global window object
   */
  @Prop( { attribute: 'search-agent' } )
  searchAgent: SearchAgent | string;
  agent: SearchAgent;

  /**
   * search result, emitted on selection
   */
  @Event( {
    eventName: 'result-selected'
  } )
  resultSelected: EventEmitter<string>;

  searchBox!: IonSearchbar;
  skipNext = false;

  @State()
  searchResults: string[] = [];

  @State()
  showResults = false;

  @Watch( 'searchAgent' )
  updateSearchAgent( newAgent: SearchAgent | string ) {
    /*DEBUG*/console.log( 'LibSearch.updateSearchAgent()' );
    if ( newAgent != undefined && typeof newAgent === 'string' ) {
      /*DEBUG*/console.log( 'agent provided as string' );
      this.agent = (window as any)[ newAgent ] as SearchAgent;
    } else {
      this.agent = newAgent as SearchAgent;
    }
  }

  componentWillLoad() {
    /*DEBUG*/console.log( 'LibSearch.componentWillLoad()' );console.log( this.searchAgent );
    this.updateSearchAgent( this.searchAgent );
  }

  async searchChanged() {
    if ( !this.skipNext ) {
      /*DEBUG*/console.log( 'searchChanged '+this.searchBox.value );
      const query = this.searchBox.value.trim().toLowerCase();
      this.searchResults = await this.agent.Search( query );
      this.showResults = ( this.searchResults.length>1 );
    }
    this.skipNext = false;
  }

  entrySelected( entry: string ) {
    /*DEBUG*/console.log( 'LibSearch.entrySelected() '+entry );
    this.searchBox.value = entry;
    this.showResults = false;
    this.skipNext = true;
    this.resultSelected.emit( entry );
  }

  render() {
    return (
      <Host>
        <ion-searchbar ref={(el) => this.searchBox = el as IonSearchbar}
                       onIonChange={() => this.searchChanged()}></ion-searchbar>
        <ion-list style={{ display: this.showResults ? 'block' : 'none'}}>
          { this.searchResults.map( ( entry ) =>
            <ion-item button={true} detail={false}
                      onClick={() => { console.log( 'entrySelected' ); this.entrySelected( entry ); }}>
              <ion-label>{entry}<div tabindex="0"></div></ion-label>
            </ion-item>
          ) }
        </ion-list>
      </Host>
    );
  }
}

# lib-search

This sample web component demonstrates an approach for UI reuse between desktop web and hybrid mobile applications where logic which may differ on the two platforms is supplied by the consuming application.

`lib-search` provides a mobile-friendly search bar. Search queries are performed against an external object that implements the included `SearchAgent` interface.

## SearchAgent

The sample `SearchAgent` interface used here is simple. Objects implementing `SearchAgent` must expose a `Search` method that takes a single string parameter and returns an array of string results.

```typescript
interface SearchAgent {
  Search: (query: string) => Promise<string[]>;
}
```

## Third Party Tools

### Ionic Framework

`lib-search` includes UI components from the Ionic Framework for their mobile compatibility. This component can be integrated into myUnity Mobile and other Ionic-based apps with little change. Desktop web consumers can conform the component to blend in with their look and feel through CSS.

[Ionic Framework](http://ionicframework.com)

### Stencil

The `lib-search` web component is implemented as a Stencil component. Stencil itself is not a required dependency for consumers; it compiles components down to framework-independent JavaScript.

Since modern Ionic Framework components are implemented with Stencil, they can be used by new Stencil components with very little friction. 

[Stencil](http://stenciljs.com)


***
<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                                                                                                                 | Type                    | Default     |
| ------------- | -------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------- | ----------- |
| `searchAgent` | `search-agent` | SearchAgent will be used to perform search queries; string values are used as keys for retrieving SearchAgent from the global window object | `SearchAgent \| string` | `undefined` |


## Events

| Event             | Description                         | Type                  |
| ----------------- | ----------------------------------- | --------------------- |
| `result-selected` | search result, emitted on selection | `CustomEvent<string>` |


## Dependencies

### Depends on

- ion-searchbar
- ion-list
- ion-item
- ion-label

### Graph
```mermaid
graph TD;
  lib-search --> ion-searchbar
  lib-search --> ion-list
  lib-search --> ion-item
  lib-search --> ion-label
  ion-searchbar --> ion-icon
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  style lib-search fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*

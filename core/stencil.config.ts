import { Config } from '@stencil/core';

export const config: Config = {
  namespace: 'sample-pluggable-component',
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader',
    },
    {
      type: 'dist-custom-elements-bundle',
    },
    {
      type: 'docs-readme',
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
      copy: [
        {
          src: '../node_modules/@ionic/core/css',
          dest: 'assets/ionic/css'
        }
      ]
    }
  ],
};
